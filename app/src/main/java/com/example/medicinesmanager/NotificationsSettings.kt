package com.example.medicinesmanager

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.time.Duration
import java.time.LocalTime
import java.util.HashMap

class NotificationsSettings : AppCompatActivity() {
    lateinit var pendingIntent: PendingIntent
    private lateinit var auth: FirebaseAuth
    private lateinit var nFirestore: FirebaseFirestore
    private var notifications: Boolean = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notifications_layout)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        auth = FirebaseAuth.getInstance()
        nFirestore = FirebaseFirestore.getInstance()
        notifications=intent.getBooleanExtra("notifications",false)
        val toggle: ToggleButton = findViewById(R.id.button)
        Toast.makeText(this,notifications.toString(),Toast.LENGTH_SHORT).show()
        toggle.isChecked = notifications
        val intent = Intent(this, ReminderBroadcast::class.java)
        pendingIntent =
            PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                notification()
    }


     fun notification() {


         if (notifications.equals(true)) {
             val intent = Intent(this, ReminderBroadcast::class.java)
             pendingIntent =
                 PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
             Log.d("time", LocalTime.now().toString())


             val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
             val quarterhour = 1000 * 60 * 10
             alarmManager.setRepeating(
                 AlarmManager.RTC_WAKEUP, 60000L,
                 60000L, pendingIntent
             )
         }
     }



    fun onClickNotificationsBtn(view: View) {
        val toggle: ToggleButton = findViewById(R.id.button)

            if (toggle.isChecked) {

                notifications=true
                saveNotificationStatus()
                notification()
//                Log.d("--- Toggle Notification  --- ", "NOTIFICATIONS ENABLED")
//                val intent = Intent(this, ReminderBroadcast::class.java)
//                pendingIntent =
//                    PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
//                Log.d("time", LocalTime.now().toString())
//
//
//
//                val alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
//                val quarterhour = 1000 * 60 * 10
//                alarmManager.setRepeating(
//                    AlarmManager.RTC_WAKEUP, 60000L,
//                    60000L , pendingIntent
//                )
            } else {
                notifications=false
                saveNotificationStatus()
                val alarmManager: AlarmManager =
                    getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.cancel(pendingIntent)

                Log.d("--- Toggle Notification  --- ", "NOTIFICATIONS DISABLED")
            }

    }


fun saveNotificationStatus() {

    Log.d("Notifications in database", notifications.toString())
    val userID = auth!!.currentUser!!.uid




    val storageMap: HashMap<String, String> = hashMapOf()
    storageMap.clear()
    storageMap.put("notification", notifications.toString())

    nFirestore?.collection("users")?.document(userID)
        ?.collection("notifications")
        ?.document("notifications")
        ?.set(storageMap)
        ?.addOnSuccessListener { Log.d("saving log", " --- It's ok ---") }
        ?.addOnFailureListener { Log.d("saving log", " --- something went wrong :( ---") }

}

    fun onClickBack(view: View) {
        val intent=Intent(this,MainMenu::class.java)
        intent.putExtra("notifications", notifications)
        startActivity(intent)
    }

    override fun onBackPressed() {
        onClickBack(view = findViewById(R.id.backBtn))
    }
}