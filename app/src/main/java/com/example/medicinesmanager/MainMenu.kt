package com.example.medicinesmanager

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import com.example.medicinesmanager.auth.AuthLogin
import com.example.medicinesmanager.dosage.DosageManager
import com.example.medicinesmanager.dosage.Dosage

import com.example.medicinesmanager.storage.Medicine
import com.example.medicinesmanager.storage.StorageMedicines

import com.example.medicinesmanager.water.WaterManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.io.File
import java.time.LocalDate
import java.time.LocalTime
import java.util.HashMap
import kotlin.math.log

private lateinit var auth: FirebaseAuth
private lateinit var name: String
private lateinit var nFirestore: FirebaseFirestore
private var water: Int = 0
private var storageMeds: ArrayList<Medicine> = ArrayList()
private var dosagesList: ArrayList<Dosage> = ArrayList()
lateinit var pendingIntent: PendingIntent
private var notifications: Boolean = false


class MainMenu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        auth = FirebaseAuth.getInstance()
        nFirestore = FirebaseFirestore.getInstance()
        val email = auth.currentUser?.email

        if (email != null) {
            name = email.split("@")[0]
            val welcomeText = findViewById<TextView>(R.id.welcomeText)
            val welcome = "Welcome " + name
            welcomeText.setText(welcome)

        }
        loadWaterData()
        loadStorageData()
        loadDosagesData()
        loadNotificationStatus()





    }

    fun onClickStorage(view: View) {
        val intent = Intent(this, StorageMedicines::class.java)
        intent.putExtra("storageMeds", storageMeds)
        startActivity(intent)
    }

    fun onClickDosage(view: View) {
        val intent = Intent(this, DosageManager::class.java)

        intent.putExtra("storageMeds", storageMeds)
        intent.putExtra("dosages", dosagesList)
        startActivity(intent)
    }

    fun onClickWater(view: View) {
        val intent = Intent(this, WaterManager::class.java)

        intent.putExtra("water", water)
        startActivity(intent)
    }


    fun onLogOut(view: View) {
        FirebaseAuth.getInstance().signOut()

        val intent = Intent(this, AuthLogin::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        FirebaseAuth.getInstance().signOut()

        val intent = Intent(this, AuthLogin::class.java)
        startActivity(intent)
    }

    fun loadWaterData() {

        val userID = auth!!.currentUser!!.uid
        val today = LocalDate.now().toString()

        val docRef = nFirestore.collection("users")?.document(userID)
            ?.collection("water")
            ?.document(today)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            if (!documentSnapshot.get("water").toString().equals("null"))

                water = Integer.parseInt(documentSnapshot.get("water").toString())
            else
                water = 0

        }


    }

    fun loadStorageData() {
        storageMeds.clear()
        val userID = auth!!.currentUser!!.uid
        nFirestore?.collection("users")?.document(userID)
            ?.collection("storage")?.get()?.addOnSuccessListener { result ->
                for (doc in result) {

                    var medicine: Medicine = Medicine(
                        doc.get("name").toString(), Integer.parseInt(
                            doc.get(
                                "quantity"
                            ).toString()
                        ),
                        LocalDate.parse(doc.get("expiration date").toString())
                    )
                    storageMeds.add(medicine)
                }
            }

    }


    fun loadDosagesData() {
        dosagesList.clear()
        val userID = auth!!.currentUser!!.uid
        nFirestore?.collection("users")?.document(userID)
            ?.collection("dosages")?.get()?.addOnSuccessListener { result ->
                for (doc in result) {
                    val med = doc.get("Medicine").toString()

//                    private var storageMeds: ArrayList<Medicine> = ArrayList()

                    val medsInDose = Integer.parseInt(doc.get("Meds in dose").toString())

                    var hoursList = ArrayList<LocalTime>()
                    val hoursListString = doc.get("Hours").toString()
                    val hours = hoursListString.split(";")
                    var hh: Int
                    var mm: Int
                    for (hour in hours) {

                        hh = Integer.parseInt(hour.split(".")[0])
                        mm = Integer.parseInt(hour.split(".")[1])
                        hoursList.add(LocalTime.of(hh, mm))
                    }
                    val endOfDosage = doc.get("Date of end").toString()
                    val dateArray = endOfDosage.split(".")
                    val DD = Integer.parseInt(dateArray[0])
                    val MM = Integer.parseInt(dateArray[1])
                    val YYYY = Integer.parseInt(dateArray[2])
                    val endDate = LocalDate.of(YYYY, MM, DD)

                    var dosage: Dosage = Dosage(med, medsInDose, hoursList, endDate)
                    dosagesList.add(dosage)
                }
            }

    }


    private fun createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "MedManagerChannel"

            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("ch1", name, importance).apply {

            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }






    fun loadNotificationStatus(){
        val userID = auth!!.currentUser!!.uid




        val docRef = nFirestore.collection("users")?.document(userID)
            ?.collection("notifications")
            ?.document("notifications")
        docRef.get().addOnSuccessListener { documentSnapshot ->
            if (!documentSnapshot.get("notification").toString().equals("null")){
             var x=documentSnapshot.get("notification").toString()
                if(x.equals("true")){
                    notifications=true
                }else
                    notifications=false

            }
            else {
                notifications = false
            }



        }
    }



    fun onClickNotificationsBtn(view: View) {
        val intent=Intent(this,NotificationsSettings::class.java)
        Log.d("-------------- notifications --------------", notifications.toString())
        intent.putExtra("notifications", notifications)
        startActivity(intent)

    }


}
