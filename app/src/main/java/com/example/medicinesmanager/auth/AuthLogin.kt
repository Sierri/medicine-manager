package com.example.medicinesmanager.auth

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.medicinesmanager.MainMenu
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase


class AuthLogin: AppCompatActivity() {

    private val TAG = "IBActivity"
    private var mAuth: FirebaseAuth? = null

    var name: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.medicinesmanager.R.layout.auth_layout)
        mAuth = FirebaseAuth.getInstance()
        if(!mAuth!!.currentUser?.email.isNullOrBlank()) {
            logged()
        }
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT



    }

    fun onClickSignInBtn(view: View?) {
        val edtUsername = findViewById<EditText>(com.example.medicinesmanager.R.id.usernameField)
        val edtPassword = findViewById<EditText>(com.example.medicinesmanager.R.id.passwordField)
        val email = edtUsername.text.toString()
        val password = edtPassword.text.toString()
        val name = email.split("@".toRegex()).toTypedArray()[0]
        this.name = name
        if (!email.isNullOrBlank()&&!password.isNullOrBlank()) {
            mAuth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(
                    this
                ) { task: Task<AuthResult?> ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "signInWithEmial:success")
                        val user = mAuth!!.currentUser

                        val intent = Intent(this, MainMenu::class.java)
                        intent.putExtra("name", name)
                        this.startActivity(intent)
                    } else {
                        Log.d(TAG, "signInWithEmial:failure", task.exception)
                        Toast.makeText(
                            applicationContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }else{
            Toast.makeText(applicationContext,"All login fields must be filled",Toast.LENGTH_SHORT).show()
        }
    }

    fun onClickRegisterBtn(view: View?) {
        val intent = Intent(this, AuthRegister::class.java)
        this.startActivity(intent)
    }

    fun logged(){
        val intent = Intent(this, MainMenu::class.java)
        intent.putExtra("name", name)
        this.startActivity(intent)
    }




}