package com.example.medicinesmanager.auth

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth


class AuthRegister : AppCompatActivity() {

    private val TAG = "IBActivity"
    private var mAuth: FirebaseAuth? = null
    var signUp: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.medicinesmanager.R.layout.register_layout)
        mAuth = FirebaseAuth.getInstance()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        signUp = findViewById(com.example.medicinesmanager.R.id.registerBtn) as Button?
    }


    fun onClickRegisterBtn(view: View?) {
        val edtEmail = findViewById(com.example.medicinesmanager.R.id.emailField) as EditText
        val edtPassword = findViewById(com.example.medicinesmanager.R.id.passwordField) as EditText
        val edtConfPassword =
            findViewById(com.example.medicinesmanager.R.id.confirmPasswordField) as EditText
        val email = edtEmail.text.toString()
        val password = edtPassword.text.toString()
        val confPassword = edtConfPassword.text.toString()
        val infoText: TextView = findViewById(com.example.medicinesmanager.R.id.infoText)
        if (email.isEmpty() || password.isEmpty() || confPassword.isEmpty()) {
            infoText.visibility = View.VISIBLE
            infoText.setText(com.example.medicinesmanager.R.string.emptyfields)
        } else if (confPassword != password) {
            infoText.visibility = View.VISIBLE
            infoText.setText(com.example.medicinesmanager.R.string.wrong_confirm_password)
        } else {
            infoText.visibility = View.INVISIBLE
            registerNewUser(email, password)
            onSuccess()
        }
    }

    fun onClickBack(view: View?) {
        val intent = Intent(this, AuthLogin::class.java)
        this.startActivity(intent)
    }

    fun registerNewUser(email: String?, password: String?) {
        mAuth!!.createUserWithEmailAndPassword(email!!, password!!)
            .addOnCompleteListener(
                this
            ) { task: Task<AuthResult?> ->
                if (task.isSuccessful) {
                    Log.d(TAG, "createUserWithEmail:success")
                    Toast.makeText(
                        applicationContext, "Konto zostało zarejestrowane.",
                        Toast.LENGTH_LONG
                    ).show()
                    val user = mAuth!!.currentUser
                } else {
                    Log.w(TAG, "CreateUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        applicationContext, "Błąd przy rejestracji konta.",
                        Toast.LENGTH_LONG
                    ).show()
                    val intent = Intent(this, AuthRegister::class.java)
                    this.startActivity(intent)
                }
            }
    }

    fun onSuccess() {
        val edtEmail = findViewById(com.example.medicinesmanager.R.id.emailField) as EditText
        val edtPassword = findViewById(com.example.medicinesmanager.R.id.passwordField) as EditText
        val edtConfPassword =
            findViewById(com.example.medicinesmanager.R.id.confirmPasswordField) as EditText
        val registerBtn: Button = findViewById(com.example.medicinesmanager.R.id.registerBtn)
        registerBtn.setVisibility(View.INVISIBLE)
        val list = ArrayList<EditText>()
        list.add(edtEmail)
        list.add(edtPassword)
        list.add(edtConfPassword)
        for (x in list) {
            x.isFocusable = false
            x.isEnabled = false
            x.isClickable = false
            x.isFocusableInTouchMode = false
        }
    }


}