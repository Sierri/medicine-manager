package com.example.medicinesmanager.dosage

import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.medicinesmanager.R
import com.example.medicinesmanager.storage.Medicine
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalTime
import java.util.*
import kotlin.collections.ArrayList

class DosageHoursDetail : AppCompatActivity() {

    private var tempObject:Dosage= Dosage()
    private var dosageList: ArrayList<Dosage> = ArrayList()
    private var medList: ArrayList<Medicine> = ArrayList()
    private lateinit var auth: FirebaseAuth
    private lateinit var nFirestore: FirebaseFirestore
    private var hourList:ArrayList<LocalTime> = ArrayList()
    private var isNew=true
    private lateinit var picker:TimePickerDialog
    var pos:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.dosage_hours_layout)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setup()
        pos=intent.getIntExtra("position", 0)
        timePicker()



    }

    fun onClickAddHourBtn(view: View) {
        val addedHour=findViewById<EditText>(R.id.addHourEditText)
        val hh=addedHour.text.toString().split(".")[0]
        val mm=addedHour.text.toString().split(".")[1]
        val hour=LocalTime.of(Integer.parseInt(hh), Integer.parseInt(mm))
        hourList.add(hour)
        tempObject.setHourList(hourList)
        val hourListView=findViewById<ListView>(R.id.dosesTimeListView)

        val adapter = DosageHourAdapter(this, hourList)
        hourListView.adapter = adapter


    }


    fun onClickBack(view: View) {
        val intent = Intent(this, DosageDetail::class.java)
        intent.putExtra("isNew", isNew)

        if (!isNew) {


            intent.putExtra("position", pos)
        }
        intent.putExtra("storageMeds", medList)
        intent.putExtra("dosages", dosageList)
        intent.putExtra("tempObject", tempObject)
        intent.putExtra("fromHours", true)


        startActivity(intent)
    }
    override fun onBackPressed() {
        onClickBack(view = findViewById(R.id.backBtn))
    }

    fun setup(){
        isNew = intent.getBooleanExtra("isNew", true)

        medList= intent.getSerializableExtra("storageMeds") as ArrayList<Medicine>
        dosageList= intent.getSerializableExtra("dosages") as ArrayList<Dosage>
        tempObject= intent.getSerializableExtra("tempObject") as Dosage

        hourList=tempObject.getHourList()

        val hourListView=findViewById<ListView>(R.id.dosesTimeListView)

        val adapter = DosageHourAdapter(this, hourList)
        hourListView.adapter = adapter

    }

    fun timePicker(){

        val addedHour=findViewById<EditText>(R.id.addHourEditText)
        addedHour.inputType=InputType.TYPE_NULL

        addedHour.setOnClickListener(View.OnClickListener {
            val cldr = Calendar.getInstance()
            val hour = cldr[Calendar.HOUR_OF_DAY]
            val minutes = cldr[Calendar.MINUTE]
            // time picker dialog
            picker = TimePickerDialog(
                this@DosageHoursDetail,
                { tp, sHour, sMinute -> addedHour.setText("$sHour.$sMinute") }, hour, minutes, true
            )
            picker.show()
        })



    }








}