package com.example.medicinesmanager.dosage

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.medicinesmanager.*
import com.example.medicinesmanager.storage.Medicine
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.ChronoUnit

class DosageManager : AppCompatActivity() {

    var dosageList: ArrayList<Dosage> = ArrayList()
    private var medList: ArrayList<Medicine> = ArrayList()
    private lateinit var auth: FirebaseAuth
    private lateinit var nFirestore: FirebaseFirestore


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dosage_managment_layout)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        auth = FirebaseAuth.getInstance()
        nFirestore = FirebaseFirestore.getInstance()
        setup()


    }

    fun getDosages(): ArrayList<Dosage> {
        return dosageList
    }
    fun checkingDosages(){
        val nowDate = LocalDate.now()
        val nowTime = LocalTime.now()

        val lastdosage: LocalDateTime
        val x =dosageList.size
        var i=0;

        while(i<dosageList.size){
            var dosage=dosageList.get(i);
            var time:LocalTime= LocalTime.of(0,0)
            val dosageLastHour:ArrayList<LocalTime> = dosage.getHourList()
            for(hour in dosage.getHourList()){
                if(ChronoUnit.MINUTES.between(time,hour)>0)
                    time=hour
            }
            Log.d("--- item ---",dosage.getMedicine())
            Log.d("--- last hour ---",time.toString())

            if(ChronoUnit.DAYS.between(nowDate,dosage.getDosageEnd())<0) {
                delete(dosage)
            }else if(ChronoUnit.DAYS.between(nowDate,dosage.getDosageEnd())==0L&&ChronoUnit.MINUTES.between(nowTime,time)<0){
                delete(dosage)

            }else{
                i++
            }

        }
    }

    fun delete(dosage:Dosage){

        val x=dosageList.indexOf(dosage)
        dosageList.removeAt(x)


        val userID = auth.currentUser!!.uid
        nFirestore?.collection("users")?.document(userID)
            ?.collection("dosages")
            ?.document(dosage.getMedicine())?.delete()
    }

    fun onClickAddBtn(view: View) {
        val intent = Intent(this, DosageDetail::class.java)
        intent.putExtra("storageMeds", medList)
        intent.putExtra("dosages", dosageList)
        intent.putExtra("isNew", true)
        intent.putExtra("fromHours",false)
        save()
        startActivity(intent)
    }

    fun onClickBack(view: View) {
        val intent = Intent(this, MainMenu::class.java)
        save()
        startActivity(intent)
    }

    override fun onBackPressed() {
        onClickBack(view = findViewById(R.id.backBtn))
    }

    private fun setup() {
        val dosageListView = findViewById<ListView>(R.id.dosageListView)
        medList = intent.getSerializableExtra("storageMeds") as ArrayList<Medicine>
        dosageList = intent.getSerializableExtra("dosages") as ArrayList<Dosage>
        Log.d(" ------------- Dosages size -------------------","\n"+dosageList.size.toString()+"\n")
        checkingDosages()



        val adapter = DosageAdapter(this, dosageList,medList)
        dosageListView.adapter = adapter
    }

    private fun save() {


        val userID = auth!!.currentUser!!.uid

        for (item in dosageList) {
            val dosageMap: HashMap<String, String> = hashMapOf()
            dosageMap.clear()
            dosageMap.put("Medicine", item.getMedicine())
            dosageMap.put("Meds in dose", item.getQuantityOfMedsInDose().toString())

            var hours: StringBuffer = StringBuffer()
            var i: Int = 0
            for (x in item.getHourList()) {

                val hh = x.hour.toString()
                val mm = x.minute.toString()
                if (i == item.getHourList().size-1) {
                    hours.append(hh + "." + mm)
                } else {
                    hours.append(hh + "." + mm + ";")
                }
                i++
            }

            dosageMap.put("Hours", hours.toString())
            var DD=item.getDosageEnd()?.dayOfMonth.toString()
            var MM= item.getDosageEnd()?.monthValue.toString()
            var YYYY= item.getDosageEnd()?.year.toString()
            dosageMap.put("Date of end", DD+"."+MM+"."+YYYY)

            nFirestore?.collection("users")?.document(userID)
                ?.collection("dosages")
                .document(item.getMedicine().toString())
                .set(dosageMap)
                .addOnSuccessListener { Log.d("saving log","-- It's ok --")}
                .addOnFailureListener { Log.d("saving log","-- Something went wrong :( --") }

        }

    }



}