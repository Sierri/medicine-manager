package com.example.medicinesmanager.dosage

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import com.example.medicinesmanager.R
import java.time.LocalTime

class DosageHourAdapter(
    private val context: Context,
    private val dataSource: ArrayList<LocalTime>
    ) : BaseAdapter() {
    private val inflater: LayoutInflater = context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.list_item_hours, parent, false)
        val deleteBtn = rowView.findViewById(R.id.btnDelete) as Button
        val hourText = rowView.findViewById<TextView>(R.id.hourText)

        val hour = getItem(position)

        hourText.setText(hour.toString())
        hourText.setPadding(0, 10, 0, 10)

        deleteBtn.setOnClickListener(View.OnClickListener() {


            dataSource.removeAt(position)
            notifyDataSetChanged()
            Log.d("---act---", "DELETE BTN CLICKED")


        })


        return rowView
    }

}