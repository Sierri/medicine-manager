package com.example.medicinesmanager.dosage

import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.medicinesmanager.R

import com.example.medicinesmanager.storage.Medicine


import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DosageDetail : AppCompatActivity() {

    val myCalendar = Calendar.getInstance()
    private var dosageList: ArrayList<Dosage> = ArrayList()
    private var medList: ArrayList<Medicine> = ArrayList()
    private lateinit var auth: FirebaseAuth
    private lateinit var nFirestore: FirebaseFirestore
    private var pos:Int=0
    private var hourList:ArrayList<LocalTime> = ArrayList()
    private var newObject:Dosage= Dosage()
    private var fromHours: Boolean = false
    private var isNew:Boolean=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dosage_managment2_layout)
        auth = FirebaseAuth.getInstance()
        nFirestore = FirebaseFirestore.getInstance()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        setup()
        dataPicker()
    }

    fun onClickEditDosesTimeBtn(view: View) {


        val intent = Intent(this, DosageHoursDetail::class.java)


        intent.putExtra("isNew",isNew)
        intent.putExtra("storageMeds", medList)
        intent.putExtra("dosages", dosageList)
        if(!isNew){
            intent.putExtra("position",pos)

            intent.putExtra("tempObject",dosageList[pos])
        }else{
            confNewObject()
            intent.putExtra("tempObject",newObject)
        }




        startActivity(intent)
    }
    fun onClickEditConfirmBtn(view: View) {
        changes()


    }


    fun onClickBack(view: View) {
        val intent = Intent(this, DosageManager::class.java)
        intent.putExtra("storageMeds", medList)
        intent.putExtra("dosages", dosageList)

        startActivity(intent)
    }
    override fun onBackPressed() {
        onClickBack(view = findViewById(R.id.backBtn))
    }

    private fun setup(){
        pos=intent.getIntExtra("position",0)

        fromHours=intent.getBooleanExtra("fromHours",false)
        medList = intent.getSerializableExtra("storageMeds") as ArrayList<Medicine>
        dosageList = intent.getSerializableExtra("dosages") as ArrayList<Dosage>
        isNew=intent.getBooleanExtra("isNew",true)

        val medName=findViewById<TextView>(R.id.name)
        val endDateView=findViewById<EditText>(R.id.endDateEditText)
        val singleDoseView=findViewById<EditText>(R.id.singleDoseEditText)
        val nameEditText=findViewById<EditText>(R.id.nameEditText)
        val hourListView=findViewById<ListView>(R.id.hoursListView)

        if(isNew){
            if(!fromHours) {                                //jest nowy ale bez godzin
                medName.setText("New dosage")
                endDateView.setHint("choose date")
                singleDoseView.setHint("pills in dose")
                nameEditText.setHint("medicine name")

            }else{                                                                                  //jest nowy ale juz z godzinami
                val tempObject=intent.getSerializableExtra("tempObject") as Dosage
                medName.setText(tempObject.getMedicine())
                endDateView.setText(tempObject.getDosageEnd().toString())
                singleDoseView.setText(tempObject.getQuantityOfMedsInDose().toString())
                nameEditText.setText(tempObject.getMedicine())
                hourList=tempObject.getHourList()

            }


            } else{                 //edytowany bez nowych godzin
            if(!fromHours) {

                medName.setText(dosageList[pos].getMedicine())
                endDateView.setText(dosageList[pos].getDosageEnd().toString())
                singleDoseView.setText(dosageList[pos].getQuantityOfMedsInDose().toString())
                nameEditText.setText(dosageList[pos].getMedicine())
                hourList = dosageList[pos].getHourList()
            }else{                                          //edytowany z nowymi godzinami
                val tempObject=intent.getSerializableExtra("tempObject") as Dosage
                medName.setText(tempObject.getMedicine())
                endDateView.setText(tempObject.getDosageEnd().toString())
                singleDoseView.setText(tempObject.getQuantityOfMedsInDose().toString())
                nameEditText.setText(tempObject.getMedicine())
                dosageList[pos].setHourList(tempObject.getHourList())
                hourList=dosageList[pos].getHourList()

            }






        }


        var itemsAdapter:ArrayAdapter<LocalTime> = ArrayAdapter(this,android.R.layout.simple_list_item_1,hourList)
        hourListView.adapter=itemsAdapter





    }





    private fun changes() {
        val isNew = intent.getBooleanExtra("isNew", true)
        val medName = findViewById<TextView>(R.id.name)
        val endDateView = findViewById<EditText>(R.id.endDateEditText)
        val singleDoseView = findViewById<EditText>(R.id.singleDoseEditText)
        val nameEditText = findViewById<EditText>(R.id.nameEditText)
        val hourListView = findViewById<ListView>(R.id.hoursListView)

        var date = endDateView.text.split("-")
        for (item in date) {
            item.replace("-", "")
        }
        val year = Integer.parseInt(date[0])
        val month = Integer.parseInt(date[1])
        val day = Integer.parseInt(date[2])
//
        if (!nameEditText.toString().isNullOrEmpty()
            && !endDateView.toString().isNullOrEmpty()
            && !singleDoseView.toString().isNullOrEmpty()
        ) {
            if (!isNew) {



                dosageList[pos].setMedicine(nameEditText.text.toString())
                dosageList[pos].setDosageEnd(LocalDate.of(year,month,day))
                dosageList[pos].setQuantityOfMedsInDose(Integer.parseInt(singleDoseView.text.toString()))

                medName.setText(nameEditText.text)
                Toast.makeText(
                    applicationContext, "Changes saved",
                    Toast.LENGTH_LONG
                ).show()
            } else {




                val newDosage=Dosage(nameEditText.text.toString(),Integer.parseInt(singleDoseView.text.toString()),hourList,LocalDate.of(year,month,day))
                dosageList.add(newDosage)
                pos=dosageList.indexOf(newDosage)

                Toast.makeText(
                applicationContext, "Dosage saved",
                Toast.LENGTH_LONG
            ).show()


        }
            save()
        }else{
            Toast.makeText(
                applicationContext, "All fields must be filled correctly",
                Toast.LENGTH_LONG
            ).show()
        }
    }


    private fun save() {



            val userID = auth!!.currentUser!!.uid



            val item:Dosage = dosageList[pos]
            val dosageMap: HashMap<String, String> = hashMapOf()
            dosageMap.clear()
            dosageMap.put("Medicine", item.getMedicine())
            dosageMap.put("Meds in dose", item.getQuantityOfMedsInDose().toString())

            var hours: StringBuffer = StringBuffer()
            var i: Int = 0
            for (x in item.getHourList()) {

                val hh = x.hour.toString()
                val mm = x.minute.toString()
                if (i == item.getHourList().size-1) {
                    hours.append(hh + "." + mm)
                } else {
                    hours.append(hh + "." + mm + ";")
                }
                i++
            }

            dosageMap.put("Hours", hours.toString())
            var DD=item.getDosageEnd()?.dayOfMonth.toString()
            var MM= item.getDosageEnd()?.monthValue.toString()
            var YYYY= item.getDosageEnd()?.year.toString()
            dosageMap.put("Date of end", DD+"."+MM+"."+YYYY)


            nFirestore?.collection("users")?.document(userID)
                ?.collection("dosages")
                .document(item.getMedicine().toString())
                .set(dosageMap)
                .addOnSuccessListener { Log.d("saving log","-- It's ok --")}
                .addOnFailureListener { Log.d("saving log","-- Something went wrong :( --") }

        }

    fun confNewObject(){
        val medName=findViewById<TextView>(R.id.name)
        val endDateView=findViewById<EditText>(R.id.endDateEditText)
        val singleDoseView=findViewById<EditText>(R.id.singleDoseEditText)
        val nameEditText=findViewById<EditText>(R.id.nameEditText)
        val hourListView=findViewById<ListView>(R.id.hoursListView)

        if(!nameEditText.text.isNullOrBlank()){
            newObject.setMedicine(nameEditText.text.toString())
        }else{
            newObject.setMedicine("")
        }
        if(!endDateView.text.isNullOrBlank()){
            var date = endDateView.text.split("-")
            for (item in date) {
                item.replace("-", "")
            }
            val year = Integer.parseInt(date[0])
            val month = Integer.parseInt(date[1])
            val day = Integer.parseInt(date[2])

            newObject.setDosageEnd(LocalDate.of(year,month,day))
        }else{
            val temp=LocalDate.now()
                newObject.setDosageEnd(temp)

        }
        if (!singleDoseView.text.isNullOrBlank()){
            newObject.setQuantityOfMedsInDose(Integer.parseInt(singleDoseView.text.toString()))
        }else{
            newObject.setQuantityOfMedsInDose(1)
        }
        newObject.setHourList(hourList)
    }

    fun dataPicker(){
        val dosageExpir=findViewById<EditText>(R.id.endDateEditText)
        val date =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub

                val today = LocalDate.now()
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
//                updateLabel()
                dosageExpir.setText(year.toString() + "-" + (myCalendar[Calendar.MONTH] + 1).toString() + "-" + dayOfMonth.toString())

            }

        dosageExpir.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                // TODO Auto-generated method stub

                DatePickerDialog(
                    this@DosageDetail, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                ).show()
            }
        })
    }



    }
