package com.example.medicinesmanager.dosage

import com.example.medicinesmanager.storage.Medicine
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

class Dosage:Serializable {

    private lateinit var medicine:String
    private var quantityOfMedsInDose:Int=0

    private lateinit var hourList:ArrayList<LocalTime>
    private lateinit var dosageEnd: LocalDate


    constructor(
        medicine: String,
        quantityOfMedsInDose: Int,

        hourList: ArrayList<LocalTime>,
        dosageEnd: LocalDate
    ) {
        this.medicine = medicine
        this.quantityOfMedsInDose = quantityOfMedsInDose

        this.hourList = hourList
        this.dosageEnd = dosageEnd
    }

    constructor()

    fun getMedicine(): String {
        return medicine
    }

    fun setMedicine(medicine: String) {
        this.medicine = medicine
    }

    fun getQuantityOfMedsInDose(): Int {
        return quantityOfMedsInDose
    }

    fun setQuantityOfMedsInDose(quantityOfMedsInDose: Int) {
        this.quantityOfMedsInDose = quantityOfMedsInDose
    }





    fun getHourList(): ArrayList<LocalTime> {
        return hourList
    }

    fun setHourList(hourList: ArrayList<LocalTime>) {
        this.hourList = hourList
    }

    fun getDosageEnd(): LocalDate? {
        return dosageEnd
    }

    fun setDosageEnd(dosageEnd: LocalDate?) {
        this.dosageEnd = dosageEnd!!
    }




    override fun toString(): String {
        return "medicine=$medicine, quantityOfMedsInDose=$quantityOfMedsInDose, hourList=$hourList, dosageEnd=$dosageEnd"
    }


}