package com.example.medicinesmanager.dosage

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.medicinesmanager.R
import com.example.medicinesmanager.storage.Medicine
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalDate
import java.time.LocalTime
import java.time.temporal.ChronoUnit

class DosageAdapter(
    private val context: Context,
    private val dataSourceDoses: ArrayList<Dosage>,
    private val dataSourceMeds: ArrayList<Medicine>
) : BaseAdapter() {
    private val inflater: LayoutInflater = context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return dataSourceDoses.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSourceDoses[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.list_item_dosage, parent, false)

        val titleTextView = rowView.findViewById(R.id.nameText) as TextView
        val infoTextView = rowView.findViewById(R.id.infoText) as TextView
        val deleteBtn = rowView.findViewById(R.id.btnDelete) as Button
        val editBtn = rowView.findViewById<Button>(R.id.editBtn)



        val dosage = getItem(position) as Dosage

// 2
        titleTextView.text = dosage.getMedicine()
        infoTextView.text = "Day of end: "+dosage.getDosageEnd().toString()
        pillsNeeded(dataSourceDoses[position],dataSourceMeds,rowView)

        titleTextView.setPadding(0, 5, 0, 0)
        infoTextView.setPadding(0, 0, 0, 10)

        deleteBtn.setOnClickListener(View.OnClickListener() {

            delete(dataSourceDoses[position].getMedicine())
            dataSourceDoses.removeAt(position)
            notifyDataSetChanged()
            Log.d("---act---", "DELETE BTN CLICKED")


        })

        editBtn.setOnClickListener {

            val intent = Intent(context, DosageDetail::class.java)
            val isNew :Boolean = false
            intent.putExtra("isNew", isNew)
            intent.putExtra("position", position)
            intent.putExtra("fromHours",false)
            intent.putExtra("storageMeds", dataSourceMeds)
            intent.putExtra("dosages", dataSourceDoses)
            ContextCompat.startActivity(context, intent, null)

            Log.d("---act---", "EDIT BTN CLICKED")


        }




        return rowView
    }
    fun delete(name: String){
        val auth: FirebaseAuth=FirebaseAuth.getInstance()
        val nFirestore: FirebaseFirestore= FirebaseFirestore.getInstance()
        val userID = auth.currentUser!!.uid
        nFirestore?.collection("users")?.document(userID)
            ?.collection("dosages")
            ?.document(name)?.delete()
    }

    fun dosesLeft(dosage:Dosage): Long {

        val dosesPerDay= dosage.getHourList().size
        val timeNow=LocalTime.now()
        val dateNow=LocalDate.now()
        val daysLeft=ChronoUnit.DAYS.between(dateNow,dosage.getDosageEnd())
        var todayLeft=0
        for(hour in dosage.getHourList())
            if(ChronoUnit.MINUTES.between(timeNow,hour)>0){
                todayLeft++
            }
        val dosesLeft=dosesPerDay*daysLeft+todayLeft
        return dosesLeft

    }
    fun pillsNeeded(dosage:Dosage, medList:ArrayList<Medicine>, rowView:View){
        val pillsText=rowView.findViewById<TextView>(R.id.pilsText)
        val doses=dosesLeft(dosage)
        val pills=doses*dosage.getQuantityOfMedsInDose()
//        +"\nDoses left: "+dosesLeft(dataSourceDoses[position])
        var outputString="Doses left: "+doses+" ("+pills+" pills)"
        var isInStorage:Boolean=false;
        var pillsQuantityInStorage:Int=0
        var pillsExpirInStorage:Boolean=true
        for (medicine in medList){
            if (medicine.getName()!!.toUpperCase().equals(dosage.getMedicine().toUpperCase()))
            {
                isInStorage=true
                pillsQuantityInStorage=medicine.getQuantity()
                if(ChronoUnit.DAYS.between(dosage.getDosageEnd(),medicine.getExpirDate())<0){
                    pillsExpirInStorage=false
                }
                    break
            }
        }

        if(!isInStorage){
            outputString+="\nTHERE IS NO MEDICINE IN THE STORAGE"
            pillsText.setTextColor(ContextCompat.getColor(context, R.color.red))
        }else if (pillsQuantityInStorage<pills){
            outputString+="\nNOT ENOUGH PILLS IN STORAGE"
            pillsText.setTextColor(ContextCompat.getColor(context, R.color.red))
        }else if (!pillsExpirInStorage){
            outputString+="\nEXPIRATION DATE OF MEDICINE IS TO SHORT"
            pillsText.setTextColor(ContextCompat.getColor(context, R.color.red))
        }
        pillsText.setText(outputString)



    }
}