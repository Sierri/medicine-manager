package com.example.medicinesmanager.water

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IntegerRes
import androidx.appcompat.app.AppCompatActivity
import com.example.medicinesmanager.MainMenu
import com.example.medicinesmanager.R
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalDate

import java.util.*
import kotlin.properties.Delegates


class WaterManager : AppCompatActivity() {

    private var nFirestore: FirebaseFirestore? = null
    private var auth: FirebaseAuth? = null

    private var dayWater by Delegates.notNull<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.water_management_layout)
        auth = FirebaseAuth.getInstance()
        nFirestore = FirebaseFirestore.getInstance()
        dayWater = intent.getIntExtra("water", 0)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setup()

    }

    fun onClickOneGlass(view: View) {


        val glassOfWater = 250 //[ml]
        val dayQuantityOfWaterText = findViewById<TextView>(R.id.drunkWaterToday)
        val dayWaterText = dayQuantityOfWaterText.text.toString().split(" ")[0]
        dayWater = Integer.parseInt(dayWaterText)
        dayWater += glassOfWater
        setup()


    }

    fun onClickAddWaterBtn(view: View) {
        val dayQuantityOfWaterText = findViewById<TextView>(R.id.drunkWaterToday)
        val addedWater = findViewById<EditText>(R.id.manualTypedWater)
        val dayWaterText = dayQuantityOfWaterText.text.toString().split(" ")[0]
        dayWater = Integer.parseInt(dayWaterText)
        dayWater += Integer.parseInt(addedWater.text.toString())
        setup()
        addedWater.text.clear()

    }

    fun onClickBack(view: View) {
        save()
        val intent = Intent(this, MainMenu::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        onClickBack(view = findViewById(R.id.backBtn))
    }

    private fun save() {


        val userID = auth!!.currentUser!!.uid
        val day = LocalDate.now().toString()
        val waterMap = hashMapOf(
            "water" to dayWater
        )

        nFirestore?.collection("users")?.document(userID)
            ?.collection("water")
            ?.document(day)
            ?.set(waterMap)
            ?.addOnSuccessListener { Log.d("saving log", " --- It's ok ---") }
            ?.addOnFailureListener { Log.d("saving log", " --- something went wrong :( ---") }


    }

    private fun setup() {

        val dayQuantityOfWaterText = findViewById<TextView>(R.id.drunkWaterToday)
        val waterText = "${dayWater} ml"
        dayQuantityOfWaterText.setText(waterText)

    }
}