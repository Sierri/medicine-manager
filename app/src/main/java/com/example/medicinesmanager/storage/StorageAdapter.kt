package com.example.medicinesmanager.storage

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.example.medicinesmanager.R


class StorageAdapter(private val context: Context,
                    private val dataSourceMeds:ArrayList<Medicine>):BaseAdapter(){
    private val inflater:LayoutInflater = context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return dataSourceMeds.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSourceMeds[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.list_item_medicine, parent, false)

        val titleTextView = rowView.findViewById(R.id.nameText) as TextView
        val infoTextView = rowView.findViewById(R.id.infoText) as TextView
        val takeOneBtn=rowView.findViewById(R.id.btnTakeOne) as Button
        val editBtn=rowView.findViewById<Button>(R.id.editBtn)

        val medicine = getItem(position) as Medicine

// 2
        titleTextView.text = medicine.getName()
        infoTextView.text="Quantity: "+medicine.getQuantity().toString()+"\nEpiration Date:\n"+medicine.getExpirDate().toString()

        titleTextView.setPadding(0,5,0,0)
        infoTextView.setPadding(0,0,0,10)

        takeOneBtn.setOnClickListener(View.OnClickListener() {

            if(medicine.getQuantity()>0) {
                medicine.setQuantity(medicine.getQuantity() - 1)
                notifyDataSetChanged()
                Log.d("---act---", "TAKEONE BTN CLICKED")
            }else{
                Toast.makeText(
                    context, "Not enough pills",
                    Toast.LENGTH_LONG
                ).show()
            }


        })

        editBtn.setOnClickListener {

            val intent = Intent(context, StorageMedicalDetail::class.java)
            intent.putExtra("isNew", false)
            intent.putExtra("position", position)
            intent.putExtra("storageMeds", dataSourceMeds)
            startActivity(context, intent, null)

            Log.d("---act---", "EDIT BTN CLICKED")


        }




        return rowView
    }
}