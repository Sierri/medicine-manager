package com.example.medicinesmanager.storage

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.example.medicinesmanager.MainMenu
import com.example.medicinesmanager.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.HashMap

private var medList: ArrayList<Medicine> = ArrayList()
private var nFirestore: FirebaseFirestore? = FirebaseFirestore.getInstance()
private var auth: FirebaseAuth? = FirebaseAuth.getInstance()



class StorageMedicines: AppCompatActivity() {
    private val new: Boolean=true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.storage_activity_layout)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        val userID = auth!!.currentUser!!.uid
//        Toast.makeText(
//            applicationContext, "UŻYTKOWNIK: "+ (auth!!.currentUser?.email.toString()),
//            Toast.LENGTH_SHORT
//        ).show()
         medList=intent.getSerializableExtra("storageMeds") as ArrayList<Medicine>

        for (item in medList)
            Log.d("lista leków",item.toString())
        setup()
    }

    fun onClickAddBtn(view: View) {
        val intent = Intent(this, StorageMedicalDetail::class.java)
        intent.putExtra("storageMeds", medList)
        intent.putExtra("isNew",true)
        startActivity(intent)
    }
    fun onClickBack(view: View) {
        Log.d("-----------","POWRÓT UDANY")
        val intent =Intent(this, MainMenu::class.java)
        save()

        startActivity(intent)
    }
    override fun onBackPressed() {
        onClickBack(view = findViewById(R.id.backBtn))
    }


   fun setup(){
       val medListView=findViewById<ListView>(R.id.medicinesListView)

       val adapter = StorageAdapter(this, medList)
       medListView.adapter=adapter



    }

    private fun save(){
Log.d("lakrstwa w storage:                          ", medList.size.toString() )
        val userID = auth!!.currentUser!!.uid

        for (item in medList){
            val storageMap: HashMap<String, String> = hashMapOf()
            storageMap.clear()
            storageMap.put("name",item.getName().toString())
            storageMap.put("quantity", item.getQuantity().toString())
            storageMap.put("expiration date", item.getExpirDate().toString())
            Log.d("--lek--", item.toString())



            nFirestore?.collection("users")?.document(userID)
                ?.collection("storage")
                ?.document(item.getName().toString())
                ?.set(storageMap)
                ?.addOnSuccessListener{ Log.d("saving log", " --- It's ok ---")}
                ?.addOnFailureListener{ Log.d("saving log", " --- something went wrong :( ---")}


        }
    }


}