package com.example.medicinesmanager.storage

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.example.medicinesmanager.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


private var medList: ArrayList<Medicine> = ArrayList()
private var itemPos: Int=0
private var nFirestore: FirebaseFirestore? = FirebaseFirestore.getInstance()
private var auth: FirebaseAuth? = FirebaseAuth.getInstance()

val myCalendar = Calendar.getInstance()

class StorageMedicalDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.storage_activity2_layout)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setup()

    }

    fun onClickEditConfirmBtn(view: View) {
        val medName=findViewById<EditText>(R.id.medicineNameEditText)
        val medQuantity=findViewById<EditText>(R.id.quantityEditText)
        val medExpiration=findViewById<EditText>(R.id.expirationDateEditText)
        val isNew=intent.getBooleanExtra("isNew", true)

        var date=medExpiration.text.split("-")
        for (item in date){
            item.replace("-","")
        }
        val year=Integer.parseInt(date[0])
        val month=Integer.parseInt(date[1])
        val day=Integer.parseInt(date[2])

        if(!medName.toString().isNullOrEmpty()&&!medQuantity.toString().isNullOrEmpty()&&!medExpiration.toString().isNullOrEmpty()) {
            if(!isNew) {
                medList[itemPos].setName(medName.text.toString())
                medList[itemPos].setQuantity(Integer.parseInt(medQuantity.text.toString()))


                medList[itemPos].setExpirDate(LocalDate.of(year,month,day))
                val medTitle = findViewById<TextView>(R.id.medicineNameTittle)
                medTitle.setText(medList[itemPos].getName())
                Toast.makeText(
                    applicationContext, "Changes saved",
                    Toast.LENGTH_LONG
                ).show()
            }else{
                val medicine: Medicine= Medicine(
                    medName.text.toString(), Integer.parseInt(
                        medQuantity.text.toString()
                    ),
                    LocalDate.of(year,month,day)
                )

                medList.add(medicine)

            }
            onClickBack(view)
        }else{
            Toast.makeText(
                applicationContext, "All fields must be filled correctly",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    fun onClickDeleteBtn(view: View) {

        delete(medList[itemPos].getName().toString())
        medList.remove(medList[itemPos])
        onClickBack(view)


    }

    fun onClickBack(view: View) {
        val intent = Intent(this, StorageMedicines::class.java)
        intent.putExtra("storageMeds", medList)
        save()
        startActivity(intent)
    }

    override fun onBackPressed() {
        onClickBack(view = findViewById(R.id.backBtn))
    }

    fun setup(){

        val isNew=intent.getBooleanExtra("isNew", true)
        val medName = findViewById<EditText>(R.id.medicineNameEditText)
        val medQuantity = findViewById<EditText>(R.id.quantityEditText)
        val medExpiration = findViewById<EditText>(R.id.expirationDateEditText)
        val medTitle = findViewById<TextView>(R.id.medicineNameTittle)
        val deleteBtn=findViewById<Button>(R.id.deleteBtn)
        medList = intent.getSerializableExtra("storageMeds") as ArrayList<Medicine>

        if(!isNew) {
            deleteBtn.isVisible=true

            itemPos = intent.getIntExtra("position", 0)
//        val medListView=findViewById<ListView>(R.id.medicinesListView)

            medTitle.setText(medList[itemPos].getName())


            medName.setText(medList[itemPos].getName())
            medQuantity.setText(medList[itemPos].getQuantity().toString())
            medExpiration.setText(medList[itemPos].getExpirDate().toString())
        }else{
            medTitle.setText(getString(R.string.new_medicine))
            deleteBtn.isInvisible=true

            medName.text.clear()
            medQuantity.text.clear()
            medExpiration.text.clear()

        }


//            DATAPICKER

        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                Log.d("dzień",dayOfMonth.toString())
                val today=LocalDate.now()
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
//                updateLabel()
                medExpiration.setText(year.toString()+"-"+ (myCalendar[Calendar.MONTH]+1).toString()+"-"+dayOfMonth.toString())
                Log.d("miesiąc",myCalendar[Calendar.MONTH].toString())
            }

        medExpiration.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                // TODO Auto-generated method stub

                DatePickerDialog(
                    this@StorageMedicalDetail, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                ).show()
            }
        })}

    fun updateLabel(){
        val myFormat="YYYY-MM-DD"
        val sdf:SimpleDateFormat= SimpleDateFormat(myFormat, Locale.GERMAN)
        val medExpiration = findViewById<EditText>(R.id.expirationDateEditText)
        medExpiration.setText(sdf.format(myCalendar.time))
        Log.d("dzien2", (myCalendar.time).toString())


    }

    private fun save(){
        val userID = auth!!.currentUser!!.uid


        for (item in medList){
            val storageMap:HashMap<String, String> = hashMapOf()
            storageMap.clear()
            storageMap.put("name",item.getName().toString())
            storageMap.put("quantity", item.getQuantity().toString())
            storageMap.put("expiration date", item.getExpirDate().toString())



            nFirestore?.collection("users")?.document(userID)
            ?.collection("storage")
            ?.document(item.getName().toString())
            ?.set(storageMap)
            ?.addOnSuccessListener{ Log.d("saving log", " --- It's ok ---")}
            ?.addOnFailureListener{ Log.d("saving log", " --- something went wrong :( ---")}


    }
    }
    fun delete(name: String){
        val userID = auth!!.currentUser!!.uid
        nFirestore?.collection("users")?.document(userID)
            ?.collection("storage")
            ?.document(name)?.delete()
    }


}