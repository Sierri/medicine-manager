package com.example.medicinesmanager.storage

import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


class Medicine:Serializable  {


    private lateinit var name: String
    private var quantity = 0
    private lateinit var expirDate: LocalDate




    constructor(name: String, quantity: Int, expirDate: LocalDate) {
        this.name = name
        this.quantity = quantity
        this.expirDate = expirDate
    }

    constructor()


    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getQuantity(): Int {
        return quantity
    }

    fun setQuantity(quantity: Int) {
        this.quantity = quantity
    }

    fun getExpirDate(): LocalDate? {
        return expirDate
    }

    fun setExpirDate(expirDate: LocalDate) {
        this.expirDate = expirDate
    }

    override fun toString(): String {
        return "Medicine{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                ", expirDate=" + expirDate +
                '}'
    }

    fun list(): ArrayList<Medicine> {
        val medList = ArrayList<Medicine>()
        val med1 = Medicine("Paracetamol", 10, LocalDate.of(2021, 5, 16))
        val med2 = Medicine("Pylargina", 15, LocalDate.of(2022, 4, 23))
        val med3 = Medicine("Witamina C", 17, LocalDate.of(2021, 6, 25))
        medList.add(med1)
        medList.add(med2)
        medList.add(med3)
        return medList
    }
}