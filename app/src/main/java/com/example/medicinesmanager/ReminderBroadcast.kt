package com.example.medicinesmanager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Chronometer
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.medicinesmanager.dosage.Dosage
import com.example.medicinesmanager.dosage.DosageManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalDate
import java.time.LocalTime

import java.time.temporal.ChronoUnit
import kotlin.random.Random
import kotlin.random.Random.Default.nextInt
import kotlin.random.nextInt

private lateinit var auth: FirebaseAuth
private lateinit var nFirestore: FirebaseFirestore

class ReminderBroadcast : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        auth = FirebaseAuth.getInstance()
        nFirestore = FirebaseFirestore.getInstance()
        Toast.makeText(context, "Notification checking", Toast.LENGTH_SHORT).show()
        var dosagesList: ArrayList<Dosage> = ArrayList()

//        val dosages = intent.getSerializableExtra("dosageList") as ArrayList<Dosage>
        dosagesList.clear()
        val userID = auth!!.currentUser!!.uid
        nFirestore?.collection("users")?.document(userID)
            ?.collection("dosages")?.get()?.addOnSuccessListener { result ->
                for (doc in result) {
                    val med = doc.get("Medicine").toString()

//                    private var storageMeds: ArrayList<Medicine> = ArrayList()

                    val medsInDose = Integer.parseInt(doc.get("Meds in dose").toString())

                    var hoursList = ArrayList<LocalTime>()
                    val hoursListString = doc.get("Hours").toString()
                    val hours = hoursListString.split(";")
                    var hh: Int
                    var mm: Int
                    for (hour in hours) {
                        hh = Integer.parseInt(hour.split(".")[0])
                        mm = Integer.parseInt(hour.split(".")[1])
                        hoursList.add(LocalTime.of(hh, mm))
                    }
                    val endOfDosage = doc.get("Date of end").toString()
                    val dateArray = endOfDosage.split(".")
                    val DD = Integer.parseInt(dateArray[0])
                    val MM = Integer.parseInt(dateArray[1])
                    val YYYY = Integer.parseInt(dateArray[2])
                    val endDate = LocalDate.of(YYYY, MM, DD)

                    var dosage: Dosage = Dosage(med, medsInDose, hoursList, endDate)
                    val nowTime = LocalTime.now()
                    for (hour in dosage.getHourList()) {
                        if (ChronoUnit.MINUTES.between(
                                hour,
                                nowTime
                            ) > 0 && ChronoUnit.MINUTES.between(hour, nowTime) < 10
                        ) {
                            Log.d(
                                "---- różnica w minutach ----- ",
                                ChronoUnit.MINUTES.between(hour, nowTime).toString()
                            )

                            val text =
                                "Please take " + dosage.getQuantityOfMedsInDose() + " pills of " + dosage.getMedicine()
                            notificate(text, context)
                            Toast.makeText(
                                context,
                                "Checking notification for " + dosage.getMedicine(),
                                Toast.LENGTH_SHORT
                            ).show()
                            break
                        }
                    }
                }
            }
    }

    fun notificate(text: String, context: Context) {
        var builder: NotificationCompat.Builder = NotificationCompat.Builder(context, "ch1")
            .setContentTitle("Dosage reminder")
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setSmallIcon(R.drawable.ic_massage)

        val notificationManager: NotificationManagerCompat = NotificationManagerCompat.from(context)
        val rand: Int = nextInt(1, 999)

        notificationManager.notify(rand, builder.build())

    }
}